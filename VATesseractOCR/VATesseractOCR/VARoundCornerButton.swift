//
//  VARoundCornerButton.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 29/01/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit

class VARoundCornerButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupButtom()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupButtom()
    }
    
    private func setupButtom() {
        let title = self.titleLabel?.text ?? "Button"
        self.setTitle(title, for: .normal)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.lightGray, for: .disabled)
        self.layer.cornerRadius = 15.0
    }
}
