//
//  VAInsuranceDetailsCellTableViewCell.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 28/01/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit

class VAInsuranceDetailsCellTableViewCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    @IBOutlet var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUpCell(withLabelText lText: String, valueText vText: String) {
        self.label.text = lText
        self.value.text = vText
    }
}
