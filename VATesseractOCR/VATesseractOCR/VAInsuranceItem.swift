//
//  VAInsuranceItem.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 28/01/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit

struct VAInsuranceItem {

    var insuranceId = "ABYZ283"
    var vehicalMake = "Volkswagen"
    var vehicalModel = "Polo"
    var vehicalCapacity = "1968cc"
    var registrationDate = "23 July 2009"
    var vehicalYearOfManufacture = "2009"
    var co2Emission = "167 g/km"
    var fuelType = "Diesel"
    var vehicalTransmissionType = "Manual"
    var vehicalColor = "Silver"
    var vehicalWeight = "3850 Kg"
    var vehicalChasisNumber = "MDMD434433452443"
    var vehicalEngineNumber = "DJGRBF85774"
    var currentInsuranceValue = "Rs. 6,54,765"
    var nextYearDepricatedValue = "Rs. 5,29,754"
    var insuranceExpiryDate = "30/04/2018"
    var insuranceType = "First Party Insurance"
    var ownerAddress = "C-702, Omega Paradise, Pune, Maharashtra"
    var ownerName = "Anthony Gonsalves"
    let KeysArray = ["Insurance ID","Owner's Name","Owner's Address", "Chassis Number", "Engine Number", "Vehicle Make", "Vehicle Model", "Vehicle Registration Date", "Year of manufacture", "Fuel Type", "CO2 Emission", "Transmission type", "Colour", "Weight", "Current Insurance Value", "Depriciated Insurance Value", "Insurance Expiry Date", "Insurance type"]
    
    //    let masterArray = ["Insurance ID": "ABYZ283","Owner's Name": "Anthony Gonsalves" ,"Owner's Address": "C-702, Omega Paradise, Pune, Maharashtra", "Chassis Number": "MDMD434433452443", "Engine Number": "DJGRBF85774", "Vehicle Make": "Volkswagen", "Vehicle Model": "Polo", "Vehicle Registration Date": "23 July 2009", "Year of manufacture": "2009", "Fuel Type": "Diesel", "CO2 Emission": "167 g/km", "Transmission type": "Manual", "Colour": "Silver", "Weight": "3850 Kg", "Current Insurance Value": "Rs. 6,54,765", "Next Year Depriciated Value": "Rs. 5,29,754", "Insurance Expiry Date": "30/04/2018", "Insurance type": "First Party Insurance"]
    
    let masterArray = ["ABYZ283","Mr Anthony Gonsalves" ,"C-702, Omega Paradise, Pune, Maharashtra","MDMD434433452443","DJGRBF85774","Honda", "Xtreme", "23 July 2016","2016","Petrol","167 g/km","Manual","Red and black","150 Kg","Rs. 1,00,765","Rs. 90,754","22/06/2017","First Party Insurance"]
}



