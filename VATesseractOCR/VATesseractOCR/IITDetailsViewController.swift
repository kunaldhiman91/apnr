//
//  IITDetailsViewController.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 28/01/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit

class IITDetailsViewController: UIViewController {

    @IBOutlet var insuranceTableView: UITableView!
     @IBOutlet var renewApplyInsurance: UIButton!
    let insuranceItem = VAInsuranceItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.insuranceTableView.dataSource = self
        self.insuranceTableView.delegate = self
        self.title = "Vehicle Details"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension IITDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 18
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let tableViewCell: VAInsuranceDetailsCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InsuranceCell") as? VAInsuranceDetailsCellTableViewCell else {
            return UITableViewCell()
        }
        
        tableViewCell.setUpCell(withLabelText:self.insuranceItem.KeysArray[indexPath.row] , valueText: self.insuranceItem.masterArray[indexPath.row])
        return tableViewCell
    }
}
