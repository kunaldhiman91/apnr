//
//  IITPurchaseViewController.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 03/02/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit

class IITPurchaseViewController: UIViewController {
    
    
    var QRImage: CIImage!
    var finalIMAGE:UIImage?
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet  var QRCodeImageView: UIImageView!
    @IBOutlet  var lblMessage: UILabel!
    
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var vehicleEngineNumberLabel: UILabel!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var vehicleMakeLabel: UILabel!
    let insuranceItem = VAInsuranceItem()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Insure It"
        finalIMAGE =  self.setUpQRCodeForAccount()
        self.setUpLabels()
        self.removeBlur()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let img = finalIMAGE else {
            return
        }
        QRCodeImageView.image = img
        
    }
    
    fileprivate func setUpQRCodeForAccount() -> UIImage? {
        if (QRImage == nil) {
            
            let infoDict = ["name":"InsureIT","sortCode":"201656","accountNumber":"29540345","insuranceAmount":"50"];
            let data = NSKeyedArchiver.archivedData(withRootObject: infoDict)
            let QRdata = data.base64EncodedString().data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            
            print("\(QRdata!)")
            if let filter = CIFilter(name: "CIQRCodeGenerator") {
                //filter.setDefaults()
                filter.setValue(QRdata!, forKey: "inputMessage")
                let transform = CGAffineTransform(scaleX: 1, y: 1)
                if let output = filter.outputImage?.transformed(by: transform) {
                    QRImage = output
                    return UIImage(ciImage:QRImage)
                }
            }
        }
        return nil
    }
    
    func removeBlur() {
        let scaleX = QRCodeImageView.frame.size.width / QRImage.extent.size.width
        let scaleY = QRCodeImageView.frame.size.height / QRImage.extent.size.height
        let transformedImage = QRImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        QRCodeImageView.image = UIImage(ciImage: transformedImage)
        
    }
    
    func setUpLabels() {
        self.licensePlateLabel.text = "MH 14 AU 9592"
        self.priceLabel.text = "£ 50"
        self.vehicleMakeLabel.text = self.insuranceItem.masterArray[5]
        self.vehicleTypeLabel.text = self.insuranceItem.masterArray[6]
        self.vehicleEngineNumberLabel.text = self.insuranceItem.masterArray[4]
    }
}
