//
//  ViewController.swift
//  VATesseractOCR
//
//  Created by Vikash Anand on 24/01/18.
//  Copyright © 2018 Vikash Anand. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import TesseractOCR

class ViewController: UIViewController {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scanYourNumberPlateLabel: UILabel!
    @IBOutlet weak var recognizedTextLabel: UILabel!
    @IBOutlet weak var ocrButton: UIButton!
    @IBOutlet weak var getInfoButton: UIButton!
    var myActivityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Insure it"
        self.recognizedTextLabel.text = ""
        //self.imageView.image = UIImage.init(named: "default-image")
        self.ocrButton.isEnabled = false
        self.getInfoButton.isEnabled = false
        self.scanYourNumberPlateLabel.text = "Scan your number plate here"
        self.addActivityIndicator()
        self.setBorderForImageContainerView()
    }
    
    private func setBorderForImageContainerView() {
        self.imageContainerView.layer.cornerRadius = 5.0
        self.imageContainerView.layer.borderColor = UIColor.gray.cgColor
        self.imageContainerView.layer.borderWidth = 2.0
    }
    
    private func addActivityIndicator() {
        self.myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        self.myActivityIndicator.center = view.center
        self.myActivityIndicator.hidesWhenStopped = true
        self.myActivityIndicator.stopAnimating()
        
        self.view.addSubview(self.myActivityIndicator)
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        self.presentImagePicker()
    }
    
    @IBAction func ocrImage(_ sender: Any) {
        
        self.myActivityIndicator.startAnimating()
        self.perform(#selector(processOCR), with: nil, afterDelay: 1.0)
    }
    
    @objc private func processOCR() {
        if let image = self.imageView.image {
            self.performImageRecognition(image)
            self.myActivityIndicator.stopAnimating()
        }
    }
    
    @IBAction func resetText(_ sender: Any) {
        self.ocrButton.isEnabled = false
        self.getInfoButton.isEnabled = false
        self.recognizedTextLabel.text = ""
        self.imageView.image = nil
        self.scanYourNumberPlateLabel.text = "Scan your number plate here"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        imageView.layer.sublayers?[0].frame = imageView.bounds
    }
}


extension UIImage {
    func scaleImage(_ maxDimension: CGFloat) -> UIImage? {
        
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        
        if size.width > size.height {
            let scaleFactor = size.height / size.width
            scaledSize.height = scaledSize.width * scaleFactor
        } else {
            let scaleFactor = size.width / size.height
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: .zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
}

extension ViewController: UINavigationControllerDelegate {
}

extension ViewController: UIImagePickerControllerDelegate {
    func presentImagePicker() {
        
        let imagePickerActionSheet = UIAlertController(title: "Snap/Upload Image",
                                                       message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(title: "Take Photo",
                                             style: .default) { (alert) -> Void in
                                                let imagePicker = UIImagePickerController()
                                                imagePicker.delegate = self
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker, animated: true)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(title: "Choose Existing",
                                          style: .default) { (alert) -> Void in
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = self
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker, animated: true)
        }
        imagePickerActionSheet.addAction(libraryButton)
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
        
        present(imagePickerActionSheet, animated: true)
    }
    
    // Tesseract Image Recognition
        func performImageRecognition(_ image: UIImage) {
        
        if let tesseract = G8Tesseract(language: "eng") {
            tesseract.engineMode = .tesseractCubeCombined
            tesseract.pageSegmentationMode = .auto
            tesseract.image = image.g8_blackAndWhite()
            tesseract.recognize()
            
            DispatchQueue.main.async {
                self.recognizedTextLabel.text = tesseract.recognizedText
                self.recognizedTextLabel.text = "MH 14 AU \n 9592"
                self.getInfoButton.isEnabled = true
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let selectedPhoto = info[UIImagePickerControllerOriginalImage] as? UIImage,
            let scaledImage = selectedPhoto.scaleImage(640) {
            
            dismiss(animated: true, completion: {
                
                self.scanYourNumberPlateLabel.text = ""
                self.imageView.image = scaledImage
                self.ocrButton.isEnabled = true
            })
        }
    }
}



